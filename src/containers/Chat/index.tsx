import React from "react";
import Header from "../../components/Header";
import { Message } from "../../types/message";
import { getMessages } from "../../services/message";
import Spinner from "../../components/Spinner";
import MessageList from "../MessageList";
import MessageInput from "../../components/MessageInput";
import EditMessageForm from "../../components/EditMessageForm";
import { Grid, Image } from "semantic-ui-react";
import logo from "../../images/logo.svg";
import { genUUID } from "../../helpers/uuid";
import { MESSAGE_ME_USER, IS_MOBILE, MESSAGE_ME_USER_ID } from "../../config";
import Copyright from "../../components/Copyright";

interface ChatState {
    requestedMessages: boolean,
    messages: Message[],
    editMessageID: string | null;
}

class Chat extends React.Component<{}, ChatState> {
    public constructor(props: {}) {
        super(props);

        this.state = {
            requestedMessages: false,
            messages: [],
            editMessageID: null
        }
    }

    public componentDidMount() {
        getMessages()
            .then((messages: Message[]): void => {
                this.setState({
                    messages,
                    requestedMessages: true
                });
            });
    }

    private addMessage(text: string): void {
        this.setState({
            messages: [
                ...this.state.messages,
                ({
                    id: genUUID(),
                    user: MESSAGE_ME_USER,
                    avatar: null,
                    text,
                    createdAt: new Date(),
                    editedAt: null,
                    likers: [],
                    likeCount: 0,
                    dislikers: [],
                    dislikeCount: 0,
                    userId: MESSAGE_ME_USER_ID
                } as Message)
            ]
        });
    }

    private showEditMessageForm(id: string): void {
        this.setState({
            editMessageID: id
        });
    }

    private hideEditMessageForm(): void {
        this.setState({
            editMessageID: null
        });
    }

    private editMessage(text: string): void {
        if (this.state.editMessageID) {
            const messages: Message[] = [...this.state.messages];
            const messageIndex: number = messages.findIndex((sMessage: Message): boolean => sMessage.id === this.state.editMessageID);

            messages[messageIndex] = {
                ...messages[messageIndex],
                text
            }

            this.setState({
                messages,
                editMessageID: null
            });
        }
    }

    private deleteMessage(id: string): void {
        const messages: Message[] = [...this.state.messages];
        const messageIndex: number = messages.findIndex((sMessage: Message): boolean => sMessage.id === id);
        messages.splice(messageIndex, 1);
        this.setState({ messages });
    }

    private likeMessage(id: string): void {
        const messages = [...this.state.messages];
        const index: number = messages.findIndex((message: Message): boolean => id === message.id);
        const diff = messages[index].likers.includes(MESSAGE_ME_USER_ID) ? -1 : 1;
        messages[index].likeCount += diff;

        if (diff > 0) {
            messages[index].likers.push(MESSAGE_ME_USER_ID);
        } else {
            messages[index].likers.splice(messages[index].likers.indexOf(MESSAGE_ME_USER_ID), 1);
        }

        this.setState({ messages });
    }

    private dislikeMessage(id: string): void {
        const messages = [...this.state.messages];
        const index: number = messages.findIndex((message: Message): boolean => id === message.id);
        const diff = messages[index].dislikers.includes(MESSAGE_ME_USER_ID) ? -1 : 1;
        messages[index].dislikeCount += diff;

        if (diff > 0) {
            messages[index].dislikers.push(MESSAGE_ME_USER_ID);
        } else {
            messages[index].dislikers.splice(messages[index].likers.indexOf(MESSAGE_ME_USER_ID), 1);
        }

        this.setState({ messages });
    }

    public render(): JSX.Element {
        const { requestedMessages, messages, editMessageID } = this.state;
        const messagesUsers: string[] = messages.map((message: Message): string => message.userId);
        const messagesUsersSet: Set<string> = new Set(messagesUsers);
        const participantsCount: number = messagesUsersSet.size;
        const logoSize: "small" | "big" | "mini" | "tiny" | "medium" | "large" | "huge" | "massive" = IS_MOBILE ? "medium" : "big";

        const messagesSortedByCreationDate: Message[] = messages.sort((message0: Message, message1: Message): number => {
            return message0.createdAt < message1.createdAt ? 1 : -1;
        });

        let latestMessageDate: Date | null = null;

        if (messages.length > 0) {
            const latestMessage: Message = messagesSortedByCreationDate[0];
            latestMessageDate = latestMessage.createdAt;
        }

        if (!requestedMessages) {
            return <Spinner />
        }

        const header: JSX.Element = <Header
            messagesCount={messages.length}
            participantsCount={participantsCount}
            latestMessageDate={latestMessageDate}
        />;

        const logoElement: JSX.Element = <Image src={logo} size={logoSize} centered className="rotating" />;

        const forms: JSX.Element = <>
            <MessageInput createMessage={(text: string): void => this.addMessage(text)} />
            {editMessageID
                ? <EditMessageForm
                    editMessage={(text: string): void => this.editMessage(text)}
                    cancelEditMessage={() => this.hideEditMessageForm()}
                    defaultMessage={
                        (messages
                            .find((sMessage: Message): boolean => sMessage.id === this.state.editMessageID) as Message)
                            .text
                    }
                />
                : ""}
        </>;

        const messagesElement: JSX.Element = <MessageList
            messages={messagesSortedByCreationDate}
            showingMessagesLimitStep={10}
            editMessage={(id: string): void => this.showEditMessageForm(id)}
            deleteMessage={(id: string): void => this.deleteMessage(id)}
            likeMessage={(id: string): void => this.likeMessage(id)}
            dislikeMessage={(id: string): void => this.dislikeMessage(id)}
        />;

        const copyright: JSX.Element = <Copyright />;


        if (IS_MOBILE) {
            return (
                <>
                    {header}
                    {logoElement}
                    {forms}
                    {messagesElement}
                    {copyright}
                </>
            );
        }

        return (
            <>
                {header}
                <Grid columns="2">
                    <Grid.Column>
                        {logoElement}
                        {forms}
                    </Grid.Column>
                    <Grid.Column>
                        {messagesElement}
                    </Grid.Column>
                </Grid>
                {copyright}
            </>
        );
    }
}

export default Chat;
