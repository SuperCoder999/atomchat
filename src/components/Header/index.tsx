import React from "react";
import { Statistic, Header as HeaderUI, Grid } from "semantic-ui-react";
import { IS_MOBILE } from "../../config";
import moment from "moment";

interface HeaderProps {
    messagesCount: number;
    participantsCount: number;
    latestMessageDate: Date | null;
}

class Header extends React.Component<HeaderProps> {
    public render(): JSX.Element {
        const statisticSize: "tiny" | "small" | "mini" | "large" | "huge" = IS_MOBILE ? "tiny" : "small";

        const peopleStatistic: JSX.Element = <Statistic size={statisticSize}>
            <Statistic.Value>{this.props.participantsCount}</Statistic.Value>
            <Statistic.Label>People</Statistic.Label>
        </Statistic>;

        if (IS_MOBILE) {
            return (
                <div className="fluid site-header">
                    <Grid columns="3" textAlign="center" verticalAlign="middle" className="fill">
                        <Grid.Column>
                           {peopleStatistic} 
                        </Grid.Column>
                        <Grid.Column>
                            <HeaderUI className="inline no-break" as="h3" style={{ color: "#0e9ad2" }}>Atom Chat</HeaderUI>
                        </Grid.Column>
                        <Grid.Column>
                            <Statistic size={statisticSize}>
                                <Statistic.Value>{this.props.messagesCount}</Statistic.Value>
                                <Statistic.Label>Messages</Statistic.Label>
                            </Statistic>
                        </Grid.Column>
                    </Grid>
                </div>
            );
        }

        return (
            <div className="fluid site-header">
                <HeaderUI as="h1" className="inline">Atom Chat</HeaderUI>
                <Statistic.Group size={statisticSize} className="inline">
                    {peopleStatistic}
                    <Statistic>
                        <Statistic.Value>{this.props.messagesCount}</Statistic.Value>
                        <Statistic.Label>
                            Messages - latest
                            {" "}
                            {this.props.latestMessageDate
                                ? moment(this.props.latestMessageDate).fromNow()
                                : ""}
                        </Statistic.Label>
                    </Statistic>
                </Statistic.Group>
            </div>
        );
    }
}

export default Header;
