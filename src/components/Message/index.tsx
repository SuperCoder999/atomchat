import React from "react";
import { Comment as Card, Icon } from "semantic-ui-react";
import { Message as MessageData } from "../../types/message";
import moment from "moment";
import { MESSAGE_ME_USER_ID } from "../../config";

interface MessageProps {
    message: MessageData;
    editMessage: (id: string) => void;
    deleteMessage: (id: string) => void;
    like: (id: string) => void;
    dislike: (id: string) => void;
}

class Message extends React.Component<MessageProps> {
    public shouldComponentUpdate(nextProps: MessageProps): boolean {
        return JSON.stringify(nextProps.message) !== JSON.stringify(this.props.message);
    }

    public render(): JSX.Element {
        const { message } = this.props;

        return (
            <Card className="message">
                {message.avatar ? <Card.Avatar src={message.avatar} title={message.user} /> : <Card.Avatar />}
                <Card.Content>
                    <Card.Author>
                        {message.user}
                    </Card.Author>
                    <Card.Text className="keep-breaks">
                        {message.text}
                    </Card.Text>
                    <Card.Actions style={{ userSelect: "none" }}>
                        {message.userId !== MESSAGE_ME_USER_ID
                            ? (
                                <>
                                    <Card.Action onClick={(): void => this.props.like(message.id)} title="Like">
                                        <Icon name="thumbs up outline" />
                                        {message.likeCount}
                                    </Card.Action>
                                    <Card.Action onClick={(): void => this.props.dislike(message.id)} title="Dislike">
                                        <Icon name="thumbs down outline" />
                                        {message.dislikeCount}
                                    </Card.Action>
                                </>
                            )
                            : ""}
                        {message.userId === MESSAGE_ME_USER_ID
                            ? (
                                <>
                                    <Card.Action onClick={(): void => this.props.editMessage(message.id)}>
                                        Edit
                                    </Card.Action>
                                    <Card.Action className="red-on-hover" onClick={(): void => this.props.deleteMessage(message.id)}>
                                        Delete
                                    </Card.Action>
                                </>
                            )
                            : ""}
                    </Card.Actions>
                    <Card.Metadata style={{ userSelect: "none" }}>
                        {" at "}
                        {moment(message.createdAt).format("LLLL")}
                        {message.editedAt ?
                            <> (edited {moment(message.editedAt).fromNow()})</>
                            : ""}
                    </Card.Metadata>
                </Card.Content>
            </Card>
        );
    }
}

export default Message;
