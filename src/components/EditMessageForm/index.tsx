import React from "react";
import { Form, Button, TextAreaProps, Modal } from "semantic-ui-react";

interface EditMessageFormProps {
    defaultMessage: string;
    editMessage: (text: string) => void;
    cancelEditMessage: () => void;
}

interface EditMessageFormState {
    newText: string;
}

class EditMessageForm extends React.Component<EditMessageFormProps, EditMessageFormState> {
    public constructor(props: EditMessageFormProps) {
        super(props);

        this.state = {
            newText: props.defaultMessage
        }
    }

    public shouldComponentUpdate(nextProps: EditMessageFormProps, nextState: EditMessageFormState): boolean {
        return nextState.newText !== this.state.newText;
    }

    private setMessage(text: string | null): void {
        this.setState({
            newText: text || ""
        });
    }

    public render(): JSX.Element {
        return (
            <Modal open>
                <Modal.Header>Update message</Modal.Header>
                <Modal.Content>
                    <Form
                        onSubmit={(event: React.FormEvent): void => {
                            if (this.state.newText) {
                                this.props.editMessage(this.state.newText);
                            }
                        }}
                        className="centered max-300"
                    >
                        <Form.TextArea
                            defaultValue={this.props.defaultMessage}
                            onChange={(event: React.FormEvent, data: TextAreaProps): void => this.setMessage((data.value as string | null))}
                            placeholder="Message"
                            autoFocus
                        />
                        <Button.Group fluid>
                            <Button primary disabled={!Boolean(this.state.newText)}>Update message</Button>
                            <Button.Or />
                            <Button secondary onClick={this.props.cancelEditMessage}>Cancel</Button>
                        </Button.Group>
                    </Form>
                </Modal.Content>
            </Modal>
        );
    }
}

export default EditMessageForm;
