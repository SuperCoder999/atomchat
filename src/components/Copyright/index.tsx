import React from "react";
import { Grid } from "semantic-ui-react";

class Copyright extends React.Component {
    public shouldComponentUpdate(): boolean {
        return false;
    }

    public render(): JSX.Element {
        return (
            <Grid columns="1" className="fluid" textAlign="center" verticalAlign="middle" style={{ marginTop: 0 }}>
                <Grid.Column style={{ padding: 0 }}>
                    Copyright &copy; 2020 Roman Melamud
                </Grid.Column>
            </Grid>
        );
    }
}

export default Copyright;
