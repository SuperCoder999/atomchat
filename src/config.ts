import { genUUID } from "./helpers/uuid";

export const GET_MESSAGES_URL = "https://api.npoint.io/b919cb46edac4c74d0a8";
export const MESSAGE_ME_USER = "Me";
export const MESSAGE_ME_USER_ID = genUUID();
const IS_MOBILE_REGEX: RegExp = /Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/;
export const IS_MOBILE: boolean = IS_MOBILE_REGEX.test(navigator.userAgent);
