import React from "react";
import Routing from "../../containers/Routing";

class Home extends React.Component {
    public render(): JSX.Element {
        return <Routing />
    }
}

export default Home;
