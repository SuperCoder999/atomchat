import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import Home from "./";
import { NotCompiledMessage } from "../../types/message";

let container: HTMLDivElement | null = null;

beforeEach((): void => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

it("should render main page", async (): Promise<void> => {
    const testMessage: NotCompiledMessage = {
        id: "80f08600-1b8f-11e8-9629-c7eca82aa7bd",
        text: "I don’t *** understand. It's the Panama accounts",
        user: "Ruth",
        avatar: "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
        userId: "9e243930-83c9-11e9-8e0c-8f1a686f4ce4",
        editedAt: "",
        createdAt: "2020-07-16T19:48:12.936Z"
    };

    jest.spyOn(window, "fetch").mockImplementationOnce((): Promise<Response> => {
        return Promise.resolve(
            new Response(
                JSON.stringify([
                    testMessage
                ]),
                {
                    headers: {
                        "Content-Type": "application/json"
                    }
                }
            )
        );
    });

    await act(async (): Promise<void> => {
        render(<Home />, container);
    });

    expect(container!.innerHTML).toContain(testMessage.text);
});

afterEach((): void => {
    unmountComponentAtNode(container!);
    container!.remove();
    container = null;
});
