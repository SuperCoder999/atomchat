/* NOTE: This server is used to serve the build folder on heroku instance */
const chalk = require("chalk");
console.warn(`Don't use ${chalk.red("npm start")} localy. Use ${chalk.green("npm run start:dev")} instead.`);

const express = require("express");
const path = require("path");
const server = express();
const buildPath = "build";

server.use(express.static(path.join(__dirname, buildPath)))

server.get("/*", (req, res) => {
    res.sendFile(path.join(__dirname, buildPath, "index.html"));
});

const port = process.env.PORT || 3000;

server.listen(port, "", () => {
    console.log(`Server is listening on port ${chalk.blueBright(port)}`);
});
